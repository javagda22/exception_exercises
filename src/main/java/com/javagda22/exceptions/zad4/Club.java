package com.javagda22.exceptions.zad4;

public class Club {

    /**
     * Komentarz
     * @param osoba - parametr
     * @throws NoAdultException - wyjątek
     */
    public void enter(Person osoba) throws NoAdultException {
        if (osoba.getWiek() < 18) {
            // rzucamy exception
            throw new NoAdultException();
        }
        System.out.println("Party hard!");
    }
}
