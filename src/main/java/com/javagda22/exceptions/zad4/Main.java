package com.javagda22.exceptions.zad4;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Club klub = new Club();
        Person person = new Person("Janusz", "Januszewski", 17);

        try {
            klub.enter(person);
        } catch (NoAdultException | IllegalArgumentException nae) {
            // obsługa tych błędów jest jednakowa
            System.out.println("Nie wpuścili mnie :(");
        }
        // obsługa wyjątku jest inna >>
        /* catch (IllegalArgumentException iae){
            System.out.println("Cos jest nietak");
        }*/
    }
}
