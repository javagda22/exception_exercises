package com.javagda22.exceptions.zad5;

import com.javagda22.exceptions.zad5.exceptions.BrakSrodkowException;
import com.javagda22.exceptions.zad5.exceptions.NiepoprawnaWartoscKwotyException;
import com.javagda22.exceptions.zad5.exceptions.ZlyNumerKontaException;

public class KontoBankowe {

    private double stanKonta;
    private String numerKonta;
    private String imieNazwiskoWlasciciela;

    public KontoBankowe(double stanKonta, String numerKonta, String imieNazwiskoWlasciciela) {
        this.stanKonta = stanKonta;
        this.numerKonta = numerKonta;
        this.imieNazwiskoWlasciciela = imieNazwiskoWlasciciela;
    }

    // pomyłka numer konta
    // brak środków
    // zła wartość kwoty

    /**
     * @param ilePrzelac
     * @param numerKontaNaKtoryPrzelewamy - na który chcemy przelać pieniądze
     */
    public void wykonajPrzelew(double ilePrzelac, String numerKontaNaKtoryPrzelewamy) throws BrakSrodkowException, NiepoprawnaWartoscKwotyException, ZlyNumerKontaException {
        if(ilePrzelac < 0){
            throw new NiepoprawnaWartoscKwotyException();
        }
        if(this.stanKonta < ilePrzelac){
            throw new BrakSrodkowException();
        }
        if(numerKontaNaKtoryPrzelewamy.length() != 6){
            throw new ZlyNumerKontaException();
        }
        this.stanKonta -= ilePrzelac;
        System.out.println("Przelewam " + ilePrzelac + " na konto " + numerKontaNaKtoryPrzelewamy);
    }

    public void zasilKonto(double ile) throws NiepoprawnaWartoscKwotyException {
        if(ile < 0){
            throw new NiepoprawnaWartoscKwotyException();
        }
        System.out.println("Wpłacam: " + ile);
        this.stanKonta += ile;
    }

    public void wyplacPieniadze(double ile) throws NiepoprawnaWartoscKwotyException, BrakSrodkowException {
        if(ile < 0){
            throw new NiepoprawnaWartoscKwotyException();
        }
        if(this.stanKonta < ile){
            throw new BrakSrodkowException();
        }
        System.out.println("Wypłacam: " + ile);
        this.stanKonta -= ile;
    }

}
