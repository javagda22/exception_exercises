package com.javagda22.exceptions.zad5;

import com.javagda22.exceptions.zad4.Person;

public class Main_Finally {
    public static void main(String[] args) {
        Object cośtam = new Person("a", "b", 2);

//        System.out.println(zrobCos(cośtam));
        System.out.println(zrobCos("tekst tekst tekst"));
    }

    public static String zrobCos(Object cośtam) {
        try {
//        if(cośtam instanceof Person){
            String person = (String) cośtam;
            //
//        }
            return person;
        } catch (ClassCastException cce) {
            System.out.println("Błąd, wystąpił wyjątek.");
        } finally {
            System.out.println("Finally!");
            // wykona się zawsze niezależnie czy catch
            // złapał wyjątek czy nie
        }
        System.out.println("Whatever!");
        return "";
    }
}
