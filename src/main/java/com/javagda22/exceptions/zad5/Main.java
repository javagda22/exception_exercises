package com.javagda22.exceptions.zad5;

import com.javagda22.exceptions.zad5.exceptions.BrakSrodkowException;
import com.javagda22.exceptions.zad5.exceptions.NiepoprawnaWartoscKwotyException;
import com.javagda22.exceptions.zad5.exceptions.ZlyNumerKontaException;

public class Main {
    public static void main(String[] args) {
        KontoBankowe kontoBankowe = new KontoBankowe(23.50, "12345", "Pawełek");

        // 6 sytuacji z błędami:
        // -- pomyłka numer konta (1 raz)
        // -- brak środków (2 razy)
        // -- zła wartość kwoty (3 razy)
        // w odpowiednich miejscach dokonać sprawdzeń w klasie KontoBankowe
        // rzucić wyjątek i obsłużyć go w Main.

        try {
            kontoBankowe.wykonajPrzelew(200, "12346");
        } catch (BrakSrodkowException e) {
            System.out.println("Brak środków na koncie. Nie można wykonać przelewu.");
            System.out.println("Komunikat: " + e.getMessage());
        } catch (NiepoprawnaWartoscKwotyException | ZlyNumerKontaException e) {
            System.out.println("Komunikat: " + e.getMessage());
        }

        // zadanie domowe:
        // w metodzie main (konto bankowe + exceptions)
        // stwórz w obsłudze błędu:
        // - jeśli użytkownik wpisał niepoprawną kwotę przelewu - poproś go o poprawienie kwoty przelewu
        // - jeśli użytkownik wpisał niepoprawny numer konta - poproś użytkownika o ponowne wpisanie numeru konta.

        // 1. na początku pobieramy kwote i numer konta na który chcemy przelać
        // 2. wywołujemy funkcję przelewu.
        // 3. w razie wyjątku obsługa:
        //   3a. jeśli użytkownik wpisał niepoprawną kwotę przelewu - poproś go o poprawienie kwoty przelewu
        //   3b. jeśli użytkownik wpisał niepoprawny numer konta - poproś użytkownika o ponowne wpisanie numeru konta.
        // 4. koniec aplikacji - komunikat "dziękuję"
    }
}
