package com.javagda22.exceptions.zad5.exceptions;

public class NiepoprawnaWartoscKwotyException extends Exception {
    public NiepoprawnaWartoscKwotyException() {
        super("Niepoprawna wartość kwoty.");
    }
}
