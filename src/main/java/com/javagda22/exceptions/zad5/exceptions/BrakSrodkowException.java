package com.javagda22.exceptions.zad5.exceptions;

public class BrakSrodkowException extends Exception{
    public BrakSrodkowException() {
        super("Brak środków na koncie.");
    }
}
