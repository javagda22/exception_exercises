package com.javagda22.exceptions.zad5.exceptions;

public class ZlyNumerKontaException extends Exception {
    public ZlyNumerKontaException() {
        super("Podany numer konta jest niepoprawny!");
    }
}
