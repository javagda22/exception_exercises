package com.javagda22.exceptions.lotto;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // a)
        int[] cyfryUzytkownika = new int[6];
//        for (int i = 0; i < cyfryUzytkownika.length; i++) {
//            cyfryUzytkownika[i] = scanner.nextInt();
//        }
        int iloscPoprawnieWpisanychLiczb = 0;
        while (iloscPoprawnieWpisanychLiczb < 6) {
            System.out.println("Proszę o wprowadzenie liczby:");
            int liczbaWpisana = scanner.nextInt();

            // sprawdzić czy w tablicy nie ma już liczby która została wpisana
            boolean czyPoprawna = true;
            for (int i = 0; i < iloscPoprawnieWpisanychLiczb; i++) {
                if(cyfryUzytkownika[i] == liczbaWpisana){
                    System.out.println("Liczba się powtarza.");
                    czyPoprawna = false;
                    break;
                }
            }
            if(czyPoprawna) {
                System.out.println("Dopisuję liczbę.");
                // wstawiam mową liczbę w tablicę
                cyfryUzytkownika[iloscPoprawnieWpisanychLiczb] = liczbaWpisana;
                // inkrementuję ilość poprawnie wpisanych liczb.
                iloscPoprawnieWpisanychLiczb++;
            }
        }

        Random generator = new Random();

        // b)
        int[] cyfryWylosowane = new int[6];
        // sposób 1
//        for (int i = 0; i < cyfryWylosowane.length; i++) {
//            cyfryWylosowane[i] = generator.nextInt(49) + 1;
//        }

        // sposób 3
        int iloscPoprawnieWylosowanychLiczb = 0;
        while (iloscPoprawnieWylosowanychLiczb < 6) {
            int liczbaWylosowana = generator.nextInt(49) + 1;

            // sprawdzić czy w tablicy nie ma już liczby która została wpisana
            boolean czyPoprawna = true;
            for (int i = 0; i < iloscPoprawnieWylosowanychLiczb; i++) {
                if(cyfryWylosowane[i] == liczbaWylosowana){
                    System.out.println("Liczba się powtarza.");
                    czyPoprawna = false;
                    break;
                }
            }
            if(czyPoprawna) {
                // wstawiam mową liczbę w tablicę
                cyfryWylosowane[iloscPoprawnieWylosowanychLiczb] = liczbaWylosowana;
                // inkrementuję ilość poprawnie wpisanych liczb.
                iloscPoprawnieWylosowanychLiczb++;
            }
        }

        // sposób 2
//        int[] podaneLiczby = new int[6];
//
//        System.out.println("Podaj 6 liczb w zakresie od 1 do 49:");
//        for (int i = 0; i < podaneLiczby.length; i++) {
//
//            podaneLiczby[i] = scanner.nextInt();
//
//            if (podaneLiczby[i] < 1 || podaneLiczby[i] > 49) {
//                System.out.println("Liczba musi mieścić się w przedziale od 1 do 49!");
//                i--;
//                continue;
//            }
//
//            for (int j = 0; j < i; j++) {
//                if (podaneLiczby[j] == podaneLiczby[i]) {
//                    System.out.println("Liczby nie mogą się powtarzać!");
//                    i--;
//                }
//            }
//        }

        // c)
        System.out.println("Wybrane: ");
        for (int i = 0; i < cyfryUzytkownika.length; i++) {
            System.out.print(cyfryUzytkownika[i] + " ");
        }

        // d)
        System.out.println();
        System.out.println("Wylosowane: ");
        for (int i = 0; i < cyfryWylosowane.length; i++) {
            System.out.print(cyfryWylosowane[i] + " ");
        }

        // e)
        // porównanie liczb.
        int iloscTrafionych = 0;

        for (int i = 0; i < cyfryWylosowane.length; i++) {
            for (int j = 0; j < cyfryUzytkownika.length; j++) {
                if (cyfryWylosowane[i] == cyfryUzytkownika[j]) {
                    iloscTrafionych++;
                }
            }
        }
        System.out.println("Ilość trafionych: " + iloscTrafionych);

    }
}
