package com.javagda22.exceptions.zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println("Równanie: " + a + "x^2 + " + b + "x + " + c + " = 0");

        try {
            double delta = (b * b) + (-4 * a * c);
            if (delta < 0) {
                throw new DeltaUjemnaException();
            }
            if (delta == 0) {
                throw new DeltaRownaZeroException();
            }

            System.out.println("Dwa pierwiastki:");

            double x1 = ((-b - (Math.sqrt(delta))) / (2 * a));
            double x2 = ((-b + (Math.sqrt(delta))) / (2 * a));

            System.out.println("X1=" + x1);
            System.out.println("X2=" + x2);
        } catch (DeltaRownaZeroException drze) {
            System.out.println("Jeden pierwiastek:");

            double x0 = ((-b) / (2 * a));
            System.out.println("X0=" + x0);
        } catch (DeltaUjemnaException due) {
            System.out.println("Zero pierwiastków");

        }
    }
}
