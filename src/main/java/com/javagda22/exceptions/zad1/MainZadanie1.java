package com.javagda22.exceptions.zad1;

import java.util.Scanner;

public class MainZadanie1 {
    public static void main(String[] args) {
        //1. Napisz program, który prosi użytkownika
        // o dwie liczby(całkowite) a i b, i wyświetla wynik dzielenia a/b.
        // Jeżeli b jest ujemne (wraz z zerem) program powinien wyświetlić
        // odpowiedni komunikat.

        //Wariant 1: Zastosuj instrukcję if
        //Wariant 2: Zastosuj instrukcję try-catch

        // * jeśli wiesz jak, spróbuj dwa warianty podzielić
        // na oddzielne metody (statyczne), z których pierwsza działa
        // wykonując wariant 1, druga wariant 2.

        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        // ściągawa:
        try {
            //--> sprawdzamy czy b <0, jeśli tak, to rzuć ArithmeticException.
            if (b < 0) {
//                throw new RuntimeException("b jest mniejsze od 0");
                throw new ArithmeticException("b jest mniejsze od 0");
            }

            System.out.println(a / b);
            // logika try
        } catch (ArithmeticException ae) {
            // obsługa błędu
            System.out.println("Niepoprawna wartość liczb: " + ae.getMessage());
        }/*catch (RuntimeException re){
            System.out.println("Niepoprawna wartość liczb: " + re.getMessage());
        }*/

        //
        // Exception - Najbardziej ogólny wyjątek
        // try{
        //      // logika
        // } catch (Exception e) {
        //      // obsługa
        // }
        // Wyjątek Exception (i wyjątki po nim dziedziczące) muszą
        // być przechwycone - inaczej kompilator zgłasza błąd.
        //
        // RuntimeException - najbardziej ogólny, z tą różnicą że jest niejawny
        //

        try {
            throw new ZleCosNapisalesException();
        } catch (ZleCosNapisalesException e) {
            e.printStackTrace();
        }
        double wynik = Math.sqrt(2.3); // < -- pierwiastek


    }
}
